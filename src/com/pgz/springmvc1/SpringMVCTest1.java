package com.pgz.springmvc1;

import com.pgz.springmvc1.beans.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.security.PrivateKey;

/**
 * 测试 SpringMVC 如何映射处理请求
 * 1、测试 @RequestMapping 映射请求注解的使用（使用在映射类和方法）。
 * 2、测试 @RequestMapping 映射请求参数、请求方法或请求头之 method。
 * 3、测试使用 params 和 headers。
 * 4、测试 @RequestMapping 映射 Ant 风格资源地址请求。
 * 5、测试 @PathVariable 可以来映射 URL 中的占位符到目标方法的参数中。
 * 6、测试 Rest 风格的 URL. 以 CRUD 为例。
 * 7、测试方法签名处使用注解绑定 请求参数。
 */
@Controller
public class SpringMVCTest1 {

    private final static String SUCCESS = "success";

    /**
     * 1、测试 @RequestMapping 映射请求注解的使用（使用在映射类和方法）
     * @RequestMapping 除了修饰方法, 还可来修饰类 ：
     * 1). 类定义处: 提供初步的请求映射信息。相对于 WEB 应用的根目录。
     * 	2). 方法处: 提供进一步的细分映射信息。 相对于类定义处的 URL。若类定义处未标注 @RequestMapping，则方法处标记的 URL相对于 WEB 应用的根目录
     * 	例：该方法处理请求-->http://localhost:8088/springmvctest1/testRequestMapping
     */
    @RequestMapping("/testRequestMapping")
    public String testRequestMapping() {
        System.out.println("测试 @RequestMapping 注解映射类和方法的使用！");
        return SUCCESS;
    }

    /**
     * 2、测试 @RequestMapping 映射请求参数、请求方法或请求头之 method
     * RequestMethod.POST、RequestMethod.GET等
     * 加上 method = RequestMethod.GET 的限制后使 请求处理更加精准
     * http://localhost:8088/springmvctest1/testRequestMapping 因请求是超链接发起的，是GET 方式，所以会使用该方法处理请求
     */
    @RequestMapping(value = "/testRequestMapping",method = RequestMethod.GET)
    public String testMethod() {
        System.out.println("testMethod");
        return SUCCESS;
    }

    /**
     * 测试 RequestMethod.POST
     * @return
     */
    @RequestMapping(value = "/testRequestMapping",method = RequestMethod.POST)
    public String testMethodPost() {
        System.out.println("testMethodPost");
        return SUCCESS;
    }
    /**
     * 3、测试使用 params 和 headers
     * 可以使用 params 和 headers 来更加精确的映射请求.
     * params 和 headers 支持简单的表达式。
     */
    @RequestMapping(value = "/testParamsAndHeaders",params = {"username","age!=10"},headers = {"Accept-Language=en-US,zh;q=0.8"})
    public String testParamsAndHeaders(){
        System.out.println("测试 params 和 headers ");
        return SUCCESS;
    }

    /**
     * 4、测试 @RequestMapping 映射 Ant 风格资源地址请求
     *  ? 匹配一个字符
     *  * 匹配文件名中的任意字符
     *  ** 匹配多层路径
     */
    @RequestMapping(value = "/testAntStyle/*/abc")
    public String testAntStyle(){
        System.out.println("测试 @RequestMapping 映射 Ant 风格资源地址请求 ");
        return SUCCESS;
    }

    /**
     * 5、测试 @PathVariable 可以来映射 URL 中的占位符到目标方法的参数中。
     * 问题1：URL 中的占位符如何表示？
     *        使用一对{}表示
     *              http://localhost:8088/springmvctest1/testPathVariable/mngx
     *              /testPathVariable/{id}
     *
     *  @PathVariable 用在数据类型前
     */

    @RequestMapping(value = "/testPathVariable/{id}")
    public String testPathVariable(@PathVariable String id){

        System.out.println("测试 @PathVariable 可以来映射 URL 中的占位符到目标方法的参数中 ");
        System.out.println("id: "+id);//mngx
        return SUCCESS;
    }

    /**
     * 6、测试 Rest 风格的 URL. 以 CRUD 为例。
     * Rest ：资源表现层转化。
     * 资源：网络上的一个实体，每种资源对应一个特定的 URL，URL 即为每一个资源的独一无二的识别符。
     * 表现层：把资源具体呈现出来的形式。
     * 状态转化：如果客户端想要操作服务器，必须通过某种手段使服务器发生转化。即对应四种操作
     * GET（获取资源）、POST（新建资源）、
     * PUT（更新资源）、DELETE（删除资源）。
     *
     * 问题1：如何发送PUT、DELETE 请求？
     * 1. 需要配置 HiddenHttpMethodFilter 。
     * 2. 需要发送 POST 请求。
     * 3. 需要在发送 POST 请求时携带一个 name="_method" 的隐藏域, 值为 DELETE 或 PUT
     */
    @RequestMapping(value = "/testRest/{id}", method = RequestMethod.PUT)
    public String testRestPut(@PathVariable Integer id) {
        System.out.println("testRest Put: " + id);
        return SUCCESS;
    }

    @RequestMapping(value = "/testRest/{id}", method = RequestMethod.DELETE)
    public String testRestDelete(@PathVariable Integer id) {
        System.out.println("testRest Delete: " + id);
        return SUCCESS;
    }

    @RequestMapping(value = "/testRest", method = RequestMethod.POST)
    public String testRest() {
        System.out.println("testRest POST");
        return SUCCESS;
    }

    @RequestMapping(value = "/testRest/{id}", method = RequestMethod.GET)
    public String testRest(@PathVariable Integer id) {
        System.out.println("testRest GET: " + id);
        return SUCCESS;
    }

    /**
     * 7、测试方法签名处使用注解绑定 请求参数。
     * 7-1、@RequestParam
     * 7-2、@RequestHeader
     * 7-3、@CookieValue
     * 7-4、使用 POJO 对象绑定请求参数值
     * 7-5、使用 Servlet API 作为入参
     */

    //7-1、@RequestParam
    @RequestMapping("/testRequestParam")
    public String testRequestParam(@RequestParam(value = "username") String un,@RequestParam("age") int age) {
        System.out.println("testRequestParam  username:" + un+"/n age:"+age);
        return SUCCESS;
    }

    //7-2、@RequestHeader
    @RequestMapping("/testRequestHeader")
    public String testRequestHeader(@RequestHeader(value = "Accept-Language") String header) {
        System.out.println("testRequestHeader  header:" + header);
        return SUCCESS;
    }

    //7-3、@CookieValue
    @RequestMapping("/testCookieValue")
    public String testCookieValue(@CookieValue(value = "JSESSIONID") String sessionid) {
        System.out.println("testCookieValue  sessionid:" + sessionid);
        return SUCCESS;
    }

    /**
     * 7-4、使用 POJO 对象绑定请求参数值
     * Spring MVC 会按请求参数名和 POJO 属性名进行自动匹配， 自动为该对象填充属性值。支持级联属性。
     */
    @RequestMapping("/testPojo")
    public String testPOJO(User user) {
        System.out.println("testPOJO  user:" + user);
        return SUCCESS;
    }

    /**
     * 7-5、可以使用 Serlvet 原生的 API 作为目标方法的参数 具体支持以下类型。
     * @param request
     * @param response
     * @param out
     * @throws IOException
     */
    @RequestMapping("/testServletAPI")
    public void testServletAPI(HttpServletRequest request,
                               HttpServletResponse response, Writer out) throws IOException {
        System.out.println("testServletAPI, " + request + ", " + response);
        out.write("hello springmvc");
    }

}
