package com.pgz.helloworld;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 相当于处理浏览器请求的控制器-Servlet
 */
@Controller
@RequestMapping("/hello")
public class HelloWorld {

    @RequestMapping("/helloSpringMVC")
    public String sayhello(){
        System.out.println("This is my first SpringMVC pragrama !");
        return "success";
    }
}
