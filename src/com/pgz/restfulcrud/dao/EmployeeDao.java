package com.pgz.restfulcrud.dao;

import com.pgz.restfulcrud.beans.Department;
import com.pgz.restfulcrud.beans.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * 模拟从数据库中获取到 Employee 信息 存放在map 中
 */
@Repository
public class EmployeeDao {
    public static Map<Integer, Employee> employees = null;


    static {
        employees = new HashMap<Integer, Employee>();
        employees.put(1001, new Employee(1001, "E-AA", "aa@163.com", 1, new Department(1001, "D-AA")));
        employees.put(1002, new Employee(1002, "E-BB", "bb@163.com", 1, new Department(1002, "D-BB")));
        employees.put(1003, new Employee(1003, "E-CC", "cc@163.com", 0, new Department(1003, "D-CC")));
        employees.put(1004, new Employee(1004, "E-DD", "dd@163.com", 0, new Department(1004, "D-DD")));
        employees.put(1005, new Employee(1005, "E-EE", "ee@163.com", 1, new Department(1005, "D-EE")));
    }

    /**
     * 1、返回所有的 employee
     * @return
     */
    public Collection<Employee> getAll() {
        return employees.values();
    }

    /**
     * 2、新增一条记录
     */
    private static Integer initId = 1006;
    @Autowired
    private DepartmentDao departmentDao;

    public void save(Employee employee) {
        if(employee.getId() == null){//如果为空则为新增,需要处理新增时的id 自增
            employee.setId(initId++);
        }

        employee.setDepartment(departmentDao.getDepartment(employee.getDepartment().getId()));
        employees.put(employee.getId(), employee);
    }

    public void delete(Integer id) {
        employees.remove(id);
    }

    public Employee getemp(Integer id) {
        return employees.get(id);
    }
}
