package com.pgz.restfulcrud.handlers;

import com.pgz.restfulcrud.beans.Department;
import com.pgz.restfulcrud.beans.Employee;
import com.pgz.restfulcrud.dao.DepartmentDao;
import com.pgz.restfulcrud.dao.EmployeeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
public class EmployeeHandler {

    @Autowired
    EmployeeDao employeeDao;
    @Autowired
    DepartmentDao departmentDao;

    /**
     * 1、查询 GET
     *
     * @param map
     * @return
     */
    @RequestMapping(value = "/emps")
    public String listemps(Map map) {
        map.put("employees", employeeDao.getAll());
        return "list";
    }

    /**
     * 显示新增页面
     *
     * @param map
     * @return
     */
    @RequestMapping(value = "/emp", method = RequestMethod.GET)
    public String addemp(Map<String, Object> map) {
        map.put("departments", departmentDao.getDepartments());
        map.put("employee", new Employee());
        return "input";
    }

    /**
     * 2、新增 POST
     *
     * @param employee
     * @return
     */
    @RequestMapping(value = "/emp", method = RequestMethod.POST)
    public String add(Employee employee) {
        employeeDao.save(employee);
        return "redirect:/crud/emps";//重定向页面
    }

    /**
     * 3、删除操作 DELETE
     */
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable Integer id) {
        employeeDao.delete(id);
        return "redirect:/crud/emps";
    }
    /**
     * 4、修改操作
     * 跳转到修改页面
     */
    @RequestMapping(value = "/emp/{id}",method = RequestMethod.GET)
    public String update_input(@PathVariable("id") Integer id,Map<String,Object> map){
        map.put("departments",departmentDao.getDepartments());
        System.out.println("Department:"+departmentDao.getDepartments());
        map.put("employee",employeeDao.getemp(id));
        return "input";
    }

    /**
     * 修改 PUT
     * @return
     */
    @RequestMapping(value = "/emp",method = RequestMethod.PUT)
    public String update(Employee employee){
        System.out.println("修改");
        employeeDao.save(employee);
        return "redirect:/crud/emps";
    }

    @ModelAttribute
    public void getemp(@RequestParam(value = "id" ,required = false) Integer id, Map<String,Object> map){
        Employee employee=employeeDao.getemp(id);
        if(employee!=null){
            map.put("employee",employee);
        }
    }

}
