<%@ page import="java.util.Map" isELIgnored="false" %>
<%@ page import="java.util.HashMap" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: pengguozhen
  Date: 2018/10/17
  Time: 11:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<%--
1、使用 spring MVC表单标签。WHY 使用 form 标签呢 ?
可以更快速的开发出表单页面, 而且可以更方便的进行表单值的回显。

2、注意：
Spring MVC 表单标签，会自动在请求域中寻找模型数据beans ，进行回显填充，支持级联。
可以通过 modelAttribute 属性指定绑定的模型属性, modelAttribute 的默认值为 command
若没有指定该属性，则默认从 request 域对象中读取 command 的表单 bean，如果该属性值也不存在，则会发生错误。

--%>

<form:form action="${pageContext.request.contextPath}/crud/emp" method="post" modelAttribute="employee">
    <c:if test="${employee.id==null}">
        LastName:<form:input path="lastName"></form:input><br>
    </c:if>
    <c:if test="${employee.id!=null}">
        <form:hidden path="id"/>
        <input type="hidden" name="_method" value="PUT">
    </c:if>

    Email:<form:input path="email"></form:input><br>
    <%
        Map<String, Object> Genders = new HashMap<String, Object>();
        Genders.put("0", "Female");
        Genders.put("1", "male");
        request.setAttribute("Genders", Genders);
    %>
    Gender:<form:radiobuttons path="gender" items="${Genders}"></form:radiobuttons><br>
    Department:
    <form:select path="department.id" items="${departments}" itemLabel="departmentName" itemValue="id">
</form:select>
    <br>
    <input type="submit" value="Submit"/><br>
</form:form>
</body>
</html>
