<%--
  Created by IntelliJ IDEA.
  User: pengguozhen
  Date: 2018/10/17
  Time: 8:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>

    <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.9.1.min.js"></script>
    <script type="text/javascript">
        $(function(){
            $(".delete").click(function(){
                var href = $(this).attr("href");
                $("form").attr("action", href).submit();
                return false;
            });
        })
    </script>
</head>

<body>
<%--
问题1：如何将 Get 请求转换为 Delete、PUT请求？
HiddenHttpMethodFilter: 只能把 POST 请求转为 DELETE、PUT 请求 ，不能转换 Get 请求。
所以要借助 JS 将 Get 请求先转换为 POST 请求。 name="_method" 是必须滴。
--%>
<form action="" method="post">
    <input type="hidden" name="_method" value="DELETE"/>
</form>


<c:if test="${empty requestScope.employees}">
    <h4>没有员工</h4>
</c:if>

<c:if test="${!empty requestScope.employees}">
    <table border="1" align="center" cellpadding="10" cellspacing="0">
        <tr align="center">
            <td>ID</td>
            <td>LastName</td>
            <td>Email</td>
            <td>Gender</td>
            <td>Department</td>
            <td>Edit</td>
            <td>Delete</td>
        </tr>
        <c:forEach items="${requestScope.employees}" var="employees">
            <tr align="center">
                <td>${employees.id}</td>
                <td>${employees.lastName}</td>
                <td>${employees.email}</td>
                <td>${employees.gender}</td>
                <td>${employees.department}</td>
                <td><a href="${pageContext.request.contextPath}/crud/emp/${employees.id}">Edit</a></td>
                <td><a class="delete" href="${pageContext.request.contextPath}/crud/delete/${employees.id}">Delete</a></td>
            </tr>
        </c:forEach>
    </table>
    <a href="${pageContext.request.contextPath}/crud/emp">Add New Employee</a>
</c:if>

</body>
<head>
    <title>Title</title>
</head>
</html>
