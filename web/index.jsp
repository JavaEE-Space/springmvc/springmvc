<%--
  Created by IntelliJ IDEA.
  User: pengguozhen
  Date: 2018/10/12
  Time: 8:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SpringMVCTest1</title>
</head>
<body>

<a href="springmvctest1/testRequestMapping">Test RequestMapping</a>
<br><br>

<a href="springmvctest1/testRequestMapping">Test Method</a>
<br><br>

<form action="springmvctest1/testRequestMapping" method="post">
    <input type="submit" value="testMethod">
</form>
<br><br>

<a href="springmvctest1/testParamsAndHeaders?username=atguigu&age=10">Test ParamsAndHeaders</a>
<br><br>

<a href="springmvctest1/testAntStyle/mngx/abc">Test AntStyle</a>
<br><br>

<a href="springmvctest1/testPathVariable/mngx">Test @PathVariable</a>
<br><br>

<form action="springmvctest1/testRest/1" method="post">
    <input type="hidden" name="_method" value="PUT"/>
    <input type="submit" value="TestRest PUT"/>
</form>
<br><br>

<form action="springmvctest1/testRest/1" method="post">
    <input type="hidden" name="_method" value="DELETE"/>
    <input type="submit" value="TestRest DELETE"/>
</form>
<br><br>

<form action="springmvctest1/testRest" method="post">
    <input type="submit" value="TestRest POST"/>
</form>
<br><br>



<a href="springmvctest1/testRequestParam?username=atguigu&age=11">Test RequestParam</a>
<br><br>

<a href="springmvctest1/testRequestHeader">Test RequestHeader</a>
<br><br>

<a href="springmvctest1/testCookieValue">Test CookieValue</a>
<br><br>

<form action="springmvctest1/testPojo" method="post">
    username: <input type="text" name="username"/>
    <br>
    password: <input type="password" name="password"/>
    <br>
    email: <input type="text" name="email"/>
    <br>
    age: <input type="text" name="age"/>
    <br>
    city: <input type="text" name="address.city"/>
    <br>
    province: <input type="text" name="address.province"/>
    <br>
    <input type="submit" value="Submit"/>
</form>
<br><br>

<a href="springmvctest1/testServletAPI">Test ServletAPI</a>
<br><br>


<a href="springmvctest2/testModelAndView">Test ModelAndView</a>
<br><br>

<a href="springmvctest2/testMap">Test Map</a>
<br><br>

<a href="springmvctest2/testSessionAttributes">Test SessionAttributes</a>
<br><br>


<!--
        测试 @ModelAttribute 注解
		模拟修改操作
		1. 原始数据为: 1, Tom, 123456,tom@atguigu.com,12
		2. 密码不能被修改.
		3. 表单回显, 模拟操作直接在表单填写对应的属性值
	-->
<form action="springmvctest2/testModelAttribute" method="Post">
    <input type="hidden" name="id" value="1"/>
    username: <input type="text" name="username" value="Tom"/>
    <br>
    email: <input type="text" name="email" value="tom@atguigu.com"/>
    <br>
    age: <input type="text" name="age" value="12"/>
    <br>
    <input type="submit" value="Submit"/>
</form>


<br><br>

</body>
</html>
